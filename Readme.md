## 文件夹结构
- config.py 配置文件，包括资源路径配置，训练参数，模型参数等
- utils.py 数据集工具，评估函数等
- model.py 模型
- trainer.py 训练流程
- main.py 主程序

## 环境
python==3.8.16
- requirments.txt 依赖库
`pip install -r requirments.txt` 安装对应依赖

## 使用
- 修改config.py
- `python main.py`

## tips
如果需要手动下载预训练模型，则需要以下五个文件
![](https://notebook-1258192674.cos.ap-beijing.myqcloud.com/img/20230804094125.png)